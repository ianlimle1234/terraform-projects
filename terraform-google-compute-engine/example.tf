# terrafrom apply -var-file=secrets.tfvars
# provider "google" {
#   credentials = var.credentials
#   project     = var.project_id
#   region      = var.region
#   zone        = var.avail_zone 
# }

# resource "google_compute_network" "myapp-vpc" {
#   name    = "${var.env_prefix}-vpc"
# }


# resource "google_compute_subnetwork" "myapp-subnet-1" {
#   name          = "${var.env_prefix}-subnet-1"
#   ip_cidr_range = var.subnet_cidr_block
#   region        = var.region
#   network       = google_compute_network.myapp-vpc.id
# }


# resource "google_compute_router" "myapp-router" {
#   name        = "${var.env_prefix}-router"
#   region      = var.region
#   project     = var.project_id
#   network     = google_compute_network.myapp-vpc.id
#     bgp {
#       asn               = 64514
#       advertise_mode    = "CUSTOM"
#       advertised_groups = ["ALL_SUBNETS"]
#       advertised_ip_ranges {
#         range = "0.0.0.0/0"
#       }
#     }
# }


# resource "google_compute_address" "myapp-address" {
#   name         = "${var.env_prefix}-address"
#   subnetwork   = google_compute_subnetwork.myapp-subnet-1.id
#   address_type = "INTERNAL"
#   purpose      = "GCE_ENDPOINT"
#   region       = var.region
# }


# resource "google_compute_router_nat" "myapp-router-nat" {
#   name                               = "${var.env_prefix}-router-nat"
#   router                             = google_compute_router.myapp-router.name
#   region                             = google_compute_router.myapp-router.region

#   nat_ip_allocate_option             = "MANUAL_ONLY"
#   nat_ips                            = google_compute_address.myapp-address.*.self_link
#   source_subnetwork_ip_ranges_to_nat = "LIST_OF_SUBNETWORKS"
  
#   subnetwork {
#     name                    = google_compute_subnetwork.myapp-subnet-1.id
#     source_ip_ranges_to_nat = ["ALL_IP_RANGES"]
#   }
  
#   log_config {
#     enable = true
#     filter = "ERRORS_ONLY"
#   }
# }

# resource "google_compute_firewall" "myapp-vpc-firewall" {
#   name    = "${var.env_prefix}-vpc-firewall"
#   network = google_compute_network.myapp-vpc.name

#   allow {
#     protocol = "icmp"
#   }

#   allow {
#     protocol = "tcp"
#     ports    = ["80", "8080", "1000-2000"]
#   }

#   source_tags = ["web"]
# }

# resource "google_compute_instance" "vm_instance" {
#   name         = "${var.env_prefix}-vm-instance"
#   machine_type = "f1-micro"
#   zone         = var.avail_zone
#   tags         = ["dev"]

#   boot_disk {
#     initialize_params {
#       image = "debian-cloud/debian-9"
#     }
#   }

#   network_interface {
#     network = google_compute_network.myapp-vpc.name
#     access_config {
#       nat_ip = google_compute_address.myapp-address.address
#     }
#   }
# }