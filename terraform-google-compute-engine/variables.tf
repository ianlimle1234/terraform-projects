variable credentials {
  description = "credentials file for GCP"
  default     = "~/.gcloud/capstoneprojs30-2b43b4aaf618.json"  
}

variable project_id {
  description = "Project ID in GCP"
  default     = "capstoneprojs30"
}

variable user {
  description = "User in GCP"
  default     = "root"
}

variable region {
  description = "Resource region in GCP"
  default     = "asia-southeast1"
}

variable avail_zone {
  description = "Resource availability zone in GCP"
  default     = "asia-southeast1-b"
}

variable alt_avail_zone {
  description = "Alternative resource availability zone in GCP"
  default     = "asia-southeast1-c"
}

variable env_prefix {
  description = "production/staging environment"
  default     = "release"
  # default     = "staging-backend"
  # default     = "staging-frontend-web"
}

# variable public_key {
#   description = "public key"
#   # default     = "~/.ssh/gcloud_id_rsa.pub"
#   type        = string
#   sensitive   = true
# }

# variable private_key {
#   description = "private key"
#   # default     = "~/.ssh/gcloud_id_rsa"
#   type        = string
#   sensitive   = true
# }
