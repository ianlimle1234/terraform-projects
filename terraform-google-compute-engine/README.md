# Google Cloud Compute Engine 

This folder contains an example of how to setup a compute engine VM instance, along with the peripheries: VPC, subnet, firewall rules, route table etc.

## How do you run this example?

1. Install [Terraform](https://www.terraform.io/).
1. Open up `variables.tf` and set secrets at the top of the file as environment variables and fill in any other variables in
   the file that don't have defaults. 
1. `terraform init`.
1. `terraform plan`.
1. `terraform apply`.