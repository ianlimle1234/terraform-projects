output "vpc-name" {
  value = data.google_compute_network.vpc.name
}

output "vpc-firewall-name" {
  value = google_compute_firewall.vpc-firewall.name
}

output "compute-instance-name" {
  value = google_compute_instance.vm_instance.name
}

output "compute-instance-internal-ip" {
  value = google_compute_instance.vm_instance.network_interface.0.network_ip
}

output "compute-instance-external-ip" {
  value = google_compute_instance.vm_instance.network_interface.0.access_config.0.nat_ip
}
