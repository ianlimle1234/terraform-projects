# terrafrom apply -var-file=secrets.tfvars
provider "google" {
  # credentials = var.credentials
  project     = var.project_id
  region      = var.region
  zone        = var.avail_zone 
}

# resource "google_compute_network" "vpc" {
#   name    = "${var.env_prefix}-vpc"
# }

data "google_compute_network" "vpc" {
  name = "default"
}

resource "google_compute_address" "static" {
  name         = "${var.env_prefix}-compute-address"
  project      = var.project_id
  region       = var.region
  depends_on = [ google_compute_firewall.vpc-firewall ]
}

resource "google_compute_firewall" "vpc-firewall" {
  name    = "${var.env_prefix}-vpc-firewall"
  network = data.google_compute_network.vpc.name

  allow {
    protocol = "all"
  }

  # allow {
  #   protocol = "tcp"
  #   ports    = ["22", "80", "443", "5000", "5001", "8080"]
  # }

  source_ranges = [ "0.0.0.0/0" ]
  target_tags   = [ var.env_prefix, "http-server", "https-server" ]
}

data "google_secret_manager_secret_version" "public_key" {
    secret = "gcloud_public_key"
}

data "google_secret_manager_secret_version" "private_key" {
    secret = "gcloud_private_key"
}

resource "google_compute_instance" "vm_instance" {
  name         = "${var.env_prefix}-vm-instance"
  machine_type = "e2-medium"
  project      = var.project_id
  zone         = var.avail_zone
  tags         = [ var.env_prefix, "http-server", "https-server" ]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }

  network_interface {
    network = data.google_compute_network.vpc.name
    access_config {
      nat_ip = google_compute_address.static.address
    }
  }

  # Copy the the SSH public Key to enable the SSH Key based authentication
  metadata = {
    ssh-keys = "${var.user}:${data.google_secret_manager_secret_version.public_key.secret_data}"
  }
  
  provisioner "remote-exec" {
    connection {
      host        = google_compute_address.static.address
      type        = "ssh"
      user        = var.user
      timeout     = "500s"
      private_key = data.google_secret_manager_secret_version.private_key.secret_data
    }
    inline = [
      # INSTALL DOCKER ENGINE & DOCKER COMPOSE
      # install docker engine: https://docs.docker.com/engine/install/debian/
      "sudo apt-get update",
      "sudo curl -fsSL https://get.docker.com -o get-docker.sh",
      "sudo sh get-docker.sh",
      # install docker compose: https://docs.docker.com/compose/install/
      "sudo curl -L \"https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)\" -o /usr/local/bin/docker-compose",
      "sudo chmod +x /usr/local/bin/docker-compose",
      "sudo docker-compose --version",

      # INSTALL GIT
      "sudo apt-get update",
      "sudo apt-get install -y git"

      # INSTALL JENKINS
      # "sudo apt-get update",
      # "sudo apt-get install -y default-jre apt-transport-https wget",
      # "java -version",
      # "wget https://pkg.jenkins.io/debian-stable/jenkins.io.key",
      # "sudo apt-key add jenkins.io.key",
      # "echo \"deb https://pkg.jenkins.io/debian-stable binary/\" | sudo tee /etc/apt/sources.list.d/jenkins.list",
      # "sudo apt-get update",
      # "sudo apt install -y jenkins",
      # "sudo systemctl start jenkins",
      # "sudo systemctl status jenkins",
      # "sudo cat /var/lib/jenkins/secrets/initialAdminPassword"

      # INSTALL NPM NODEJS
      # "sudo apt update && sudo apt -y upgrade",
      # "sudo apt install -y curl",
      # "curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh",
      # "sudo bash nodesource_setup.sh",
      # "sudo apt install -y nodejs",
      # "sudo apt install -y build-essential",
      # "sudo su",
      # "npm install -g serve"
    ]
  }
  
  # Ensure firewall rule is provisioned before server, so that SSH doesn't fail.
  depends_on = [ google_compute_firewall.vpc-firewall ]
  
  allow_stopping_for_update = true
  deletion_protection       = false 
}
