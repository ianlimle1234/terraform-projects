variable credentials {
  description = "credentials file for GCP"
  default     = "~/.gcloud/capstoneprojs30-2b43b4aaf618.json"  
}

variable project_id {
  description = "Project ID in GCP"
  default     = "capstoneprojs30"
}

variable region {
  description = "Resource region in GCP"
  default     = "asia-southeast1"
}

variable avail_zone {
  description = "Resource availability zone in GCP"
  default     = "asia-southeast1-b"
}

variable alt_avail_zone {
  description = "Alternative resource availability zone in GCP"
  default     = "asia-southeast1-c"
}

variable env_prefix {
  description = "production/staging environment"
  default     = "staging"
}