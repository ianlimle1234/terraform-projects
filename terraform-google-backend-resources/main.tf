# terrafrom apply -var-file=secrets.tfvars
provider "google" {
  project     = var.project_id
  region      = var.region
  zone        = var.avail_zone 
}


resource "google_compute_network" "private_network" {
  provider = google
  name     = "${var.env_prefix}-private-network"
}


resource "google_compute_global_address" "private_ip_address" {
  provider      = google
  name          = "${var.env_prefix}-private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  network       = google_compute_network.private_network.id
  project       = var.project_id
}


resource "google_service_networking_connection" "private_vpc_connection" {
  provider                = google
  network                 = google_compute_network.private_network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}


# create serverless VPC access connecting App Engine to VPC network
resource "google_vpc_access_connector" "connector" {
  name          = "${var.env_prefix}-connector"
  ip_cidr_range = "10.16.0.0/28"
  network       = "default"
  region        = var.region
  project       = var.project_id
}


resource "google_sql_user" "users" {
  host     = "%"
  name     = "root"
  password = "root"
  project  = var.project_id
  instance = google_sql_database_instance.sql-instance.name
}


resource "google_sql_database" "hotel" {
  name      = "hotel_db"
  charset   = "utf8"
  collation = "utf8_general_ci"
  project   = var.project_id
  instance  = google_sql_database_instance.sql-instance.name
}


resource "google_sql_database" "paypal" {
  name      = "paypal_db"
  charset   = "utf8"
  collation = "utf8_general_ci"
  project   = var.project_id
  instance  = google_sql_database_instance.sql-instance.name
}


resource "google_sql_database_instance" "sql-instance" {
  provider             = google
  name                 = "${var.env_prefix}-sql-instance"
  region               = var.region
  project              = var.project_id
  deletion_protection  = true
  database_version     = "MYSQL_5_7"

  settings {
    tier = "db-f1-micro"
    backup_configuration {
      binary_log_enabled = true
      enabled            = true
    }
    ip_configuration {
      ipv4_enabled    = false
      private_network = google_compute_network.private_network.id
      require_ssl     = false
    }
  }

  depends_on = [google_service_networking_connection.private_vpc_connection]
}


resource "google_redis_instance" "hotel" {
  name                    = "${var.env_prefix}-hotel-redis"
  memory_size_gb          = 1
  project                 = var.project_id
  region                  = var.region
  location_id             = var.avail_zone
  alternative_location_id = var.alt_avail_zone

  authorized_network = google_compute_network.private_network.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  tier              = "STANDARD_HA"
  display_name      = "${var.env_prefix}-hotel-redis"

  depends_on = [google_service_networking_connection.private_vpc_connection]
}


resource "google_redis_instance" "paypal" {
  name                    = "${var.env_prefix}-paypal-redis"
  memory_size_gb          = 1
  project                 = var.project_id
  region                  = var.region
  location_id             = var.avail_zone
  alternative_location_id = var.alt_avail_zone

  authorized_network = google_compute_network.private_network.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  tier              = "STANDARD_HA"
  display_name      = "${var.env_prefix}-paypal-redis"

  depends_on = [google_service_networking_connection.private_vpc_connection]
}