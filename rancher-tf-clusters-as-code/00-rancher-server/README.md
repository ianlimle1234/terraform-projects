# Deploy Rancher in a k3s Cluster using Helm
This repository contains source code to deploy a Rancher server. 

## Suggested Architecture
* Kubernetes distribution: k3s (RKE Terraform provider isn't under Rancher SLA)
* K3s datastore: Amazon Aurora, MySQL 5.7 (external HA datastore)
* 2 k3s master nodes (High-Availability)
* Rancher SSL provided by cert-manager and Let's Encrypt
* Layer 4/Network Load Balancer in front of Rancher server (allows certs provided by Ingress Controller to pass through, no concept of encryption at layer 4 level)

## Prerequisites
* [Terraform](https://www.terraform.io/downloads.html)

## Project Structure
```
├── README.md
└── terraform
    └── modules/install-rancher
        └── main.tf
        └── outputs.tf
        └── variables.tf
        └── versions.tf
    └── root/rancher
        └── main.tf
        └── outputs.tf
        └── variables.tf
        └── versions.tf

```

