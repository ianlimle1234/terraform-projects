provider "helm" {
  kubernetes {
    config_path = var.config_path
  }
}

provider "kubernetes" {
  config_path = var.config_path
}

provider "rancher2" {
  alias     = "boostrap"
  api_url   = "https://${var.rancher_hostname}"
  bootstrap = true
}

module "install_rancher" {
  source = "../../modules/install-rancher"

  providers = {
    helm              = helm
    kubernetes        = kubernetes
    rancher2.boostrap = rancher2.boostrap
  }

  rancher_hostname = var.rancher_hostname
  rancher_password = var.rancher_password
  rancher_sets     = [
    {
      "name"  = "ingress.tls.source",
      "value" = "letsEncrypt",
      "type"  = "auto",
    },
    {
      "name"  = "letsEncrypt.email",
      "value" = "ianlimle1234@gmail.com",
      "type"  = "auto",
    },
    {
      "name"  = "replicas",
      "value" = "1",
      "type"  = "auto",
    },
  ]
}

