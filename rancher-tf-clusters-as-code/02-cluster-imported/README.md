# Create an imported cluster - basic AKS
This repository contains source code to create a rancher2_cluster without rke_config argument. 

## Suggested Architecture
* Create rancher2_cluster wihtout specifying a config field (rke_config, aks_config etc).
* Fetch import command from cluster.
* Create Job in target kubernetes cluster to perform the apply (No nice way to perform kubectl apply through tf directly)

## Prerequisites
* [Terraform](https://www.terraform.io/downloads.html)

## Project Structure
```
├── README.md
├── terraform
|    └── modules/basic-aks-cluster
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── modules/imported-cluster
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/cluster
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/imported
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── state
|        └── azure-creds.tfvars.example
|        └── rancher-creds.tfvars.example
├── apply.sh
└── destroy.sh
```