# Configure cluster
This repository contains source code to configure clusters. 

## Suggested Architecture
* Install Longhorn for generic, low-maintenance storage needs.
* Provide logging configuration by default for observability.
* Install cert-manager to allow easy management of all certificates.

## Prerequisites
* [Terraform](https://www.terraform.io/downloads.html)

## Project Structure
```
├── README.md
├── terraform
|    └── modules/default-tooling
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── modules/team-project
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/project
|        └── main.tf
|        └── variables.tf
|    └── root/tooling
|        └── main.tf
|        └── variables.tf
|        └── versions.tf
|    └── state
|        └── do-creds.tfvars.example
|        └── rancher-creds.tfvars.example
├── apply-vsphere.sh
├── apply.sh
├── destroy-vsphere.sh
└── destroy.sh
```