# Create a custom cluster - private EC2
This repository contains source code to create a rancher2_cluster using rke_config argument. 

## Suggested Architecture
* Create rancher2_cluster using rke_config argument.
* Fetch docker command from rancher2_cluster resource.
* Create master & worker EC2 instances in private subnet (Egress allowed via NAT gateway)
* Create ingress worker EC2 instance in public subnet.

## Prerequisites
* [Terraform](https://www.terraform.io/downloads.html)

## Project Structure
```
├── README.md
├── terraform
|    └── modules/private-aws-cluster
|        └── files
|            └── userdata_ec2_ingress.template
|            └── userdata_ec2_internal.template
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/cluster
|        └── main.tf
|        └── outputs.tf
|        └── variables.tf
|        └── versions.tf
|    └── state
|        └── aws-creds.tfvars.example
|        └── rancher-creds.tfvars.example
├── apply.sh
└── destroy.sh
```