## Set up Rancher Server on AWS
Two single-node Kubernetes clusters will be created from two EC2 instances running Ubuntu 18.04 and Docker, with one running Rancher and the other ready for experimentation deployments. Both instances will have wide-open security groups and will be accessible over SSH using the SSH keys `id_rsa` and `id_rsa.pub`.

*Please note while this setup is great to explore Rancher, a production setup should follow high availability setup guidelines*

### 1. Edit `sensitive.tfvars` and customize the following variables
```
aws_access_key=
aws_secret_key=
rancher_server_admin_password=
```
### 2. Edit optional variables within `variables.tf`
- `aws_region`: Amazon AWS region, chose the closest instead of the default
- `prefix`: prefix for all created resources
- `instance_type`: EC2 instance size used, minimum is `t3a.medium`
### 3. Run
```
terraform init
terraform plan
terraform apply --auto-approve
```
### 4. Verify deployment
From the terminal output, paste the `rancher_server_url` into the browser. Log in when prompted (default password is `admin`, sue the password set in `rancher_server_admin_password`).



