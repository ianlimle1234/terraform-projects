# Create an node driver cluster - basic DigitalOcean
This repository contains source code to create a rancher2_cluster using node drivers. 

## Suggested Architecture
* Create cloud credential once.
* Reuse credential for multiple node templates.
* Reuse node templates across multiple clusters.
* Create rancher2_cluster and specify rke_config.
* Using given node pool config, create node pools.

## Prerequisites
* [Terraform](https://www.terraform.io/downloads.html)

## Project Structure
```
├── README.md
├── terraform
|    └── modules/do-node-driver-cluster
|        └── main.tf
|        └── variables.tf
|        └── versions.tf
|    └── modules/generic-node-driver-cluster
|        └── main.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/cluster
|        └── main.tf
|        └── variables.tf
|        └── versions.tf
|    └── root/vsphere-cluster
|        └── main.tf
|        └── variables.tf
|        └── versions.tf
|    └── state
|        └── do-creds.tfvars.example
|        └── rancher-creds.tfvars.example
├── apply-vsphere.sh
├── apply.sh
├── destroy-vsphere.sh
└── destroy.sh
```