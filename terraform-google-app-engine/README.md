# Google Cloud App Engine

This folder contains an example of how to setup a App Engine

## How do you run this example?

### Run as resource
1. Install [Terraform](https://www.terraform.io/).
1. Open up `variables.tf` and set secrets at the top of the file as environment variables and fill in any other variables in
   the file that don't have defaults. 
1. `terraform init`.
1. `terraform plan`.
1. `terraform apply`.

### Run as module
```hcl
module "appengine" {
  source  = "JamesWoolfenden/appengine/gcp"
  version = "0.1.2"
  common_labels = var.common_labels
  project     = var.project
  app         = var.app
  sourcezip   = var.sourcezip
}

variable "common_labels" {
  description = "tags to your cloud objects"
  default     = {
  }
}

variable "project" {
  default = "PROJECT_ID"
}

variable "app" {
  description = "map of all the service properties"
  default     = {
  }
}

variable "sourcezip" {
  description = "source zip file payload for app engine"
  default     = "PATH/TO/SOURCE/ZIP/FILE"
}
```
