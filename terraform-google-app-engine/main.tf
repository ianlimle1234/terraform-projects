# terrafrom apply -var-file=secrets.tfvars
provider "google" {
  credentials = var.credentials
  project     = var.project_id
  region      = var.region
  zone        = var.avail_zone 
}

resource "google_app_engine_application" "app" {
  project     = var.project_id
  location_id = var.region
}

resource "google_project_service" "service" {
  project = var.project_id
  service = "appengineflex.googleapis.com"

  disable_dependent_services = false
}

resource "google_app_engine_standard_app_version" "paypalServer_v1" {
  version_id = "v1"
  service    = "${var.env_prefix}-paypalServer"
  runtime    = "go115"

  entrypoint {
    shell = "go run main.go"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
    }
  }

  env_variables = {
    REDISPORT="6379",
    REDISHOST="redis-session",
    SERVERPORT="8081",
    DBNAME="kyc_db",
    DBUSERNAME="root",
    DBPASSWORD="root",
    DBPORT="3306",
    DBHOST="kyc-db",
    ENV="development",
    CLIENTID="12345",
    CLIENTSECRET="12345"
  }

  automatic_scaling {
    max_concurrent_requests = 10
    min_idle_instances = 1
    max_idle_instances = 3
    min_pending_latency = "1s"
    max_pending_latency = "5s"
    standard_scheduler_settings {
      target_cpu_utilization = 0.5
      target_throughput_utilization = 0.75
      min_instances = 2
      max_instances = 10
      }
  }
  # service will be deleted if it is the last version
  delete_service_on_destroy = true
  instance_class            = "F1"
}

resource "google_app_engine_standard_app_version" "hotelServer_v1" {
  version_id = "v1"
  service    = "${var.env_prefix}-hotelServer"
  runtime    = "go115"

  entrypoint {
    shell = "go run main.go"
  }

  deployment {
    zip {
      source_url = "https://storage.googleapis.com/${google_storage_bucket.bucket.name}/${google_storage_bucket_object.object.name}"
    }
  }

  env_variables = {
    REDISPORT="6379",
    REDISHOST="redis-session",
    SERVERPORT="8081",
    DBNAME="kyc_db",
    DBUSERNAME="root",
    DBPASSWORD="root",
    DBPORT="3306",
    DBHOST="kyc-db",
    ENV="development",
    CLIENTID="12345",
    CLIENTSECRET="12345"
  }

  basic_scaling {
    max_instances = 3
  }
  
  # application version will not be deleted
  noop_on_destroy = true
  instance_class  = "B1"
  
}

resource "google_storage_bucket" "bucket" {
  name = "${var.env_prefix}-appengine-static-content"
}

resource "google_storage_bucket_object" "object" {
  name   = "PayPal-Offline-KYC.zip"
  bucket = google_storage_bucket.bucket.name
  source = "../../PayPal-Offline-KYC.zip"
}