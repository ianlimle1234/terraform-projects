variable credentials {
  description = "credentials file for GCP"
  default     = "~/.gcloud/capstoneprojs30-2b43b4aaf618.json"  
}

variable project_id {
  description = "Project ID in GCP"
  default     = "capstoneprojs30"
}

variable region {
  description = "Resource region in GCP"
  default     = "asia-southeast1"
}

variable avail_zone {
  description = "Resource availability zone in GCP"
  default     = "asia-southeast1-b"
}

variable env_prefix {
  description = "prod/stage environment"
  default     = "stage"
}
