generate "provider" {
  path = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents = <<EOF
  provider "aws" {
     region  = "ap-southeast-1"
     profile = "default"
  }
  terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "2.57"
    }
}
  required_version = "~> 1.0.4"
  backend "s3" {}
  }
EOF
}

remote_state {
  backend = "s3"
  config = {
    encrypt                 = true
    bucket                  = "tf-rke-k8s-remote-state" # your s3 bucket name containing the terraform remote backend state config
    key                     = "${path_relative_to_include()}/terraform.tfstate"
    dynamodb_table          = "tf-lock-table" # your dynamodb table name
    profile                 = "default" # your aws profile
    region                  = "ap-southeast-1" # your aws region
  }
}