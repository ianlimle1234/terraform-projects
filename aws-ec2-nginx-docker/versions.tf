terraform {
  required_version = ">=1.0.4"
  backend "s3" {
    bucket = "myapp-s3bucket"
    key    = "myapp/state.tfstate"
    region = "ap-southeast-1"
  }
}